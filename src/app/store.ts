import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import calendarReducer from '../features/calendar/calendar.slice';
import eventsReducer from '../features/events/events.slice';

export const store = configureStore({
  reducer: {
    calendar: calendarReducer,
    events: eventsReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
