import React from "react";
import { Calendar } from "./features/calendar/calendar.component";
import "./App.css";
import { makeStyles, createStyles } from "@material-ui/core";
const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: "100vw",
      height: "100vh",
      minWidth: "100vw",
      minHeight: "100vh",
      padding:24
    },
  })
);
function App() {
  const classnames = useStyles();
  return (
    <div className={classnames.root}>
      <Calendar />
    </div>
  );
}

export default App;
