import { DateEvent } from "../events/events.interfaces";

export interface CalendarCell {
  title: string;
  secondaryTitle?: string;

  /**
   * If the cell is coresponds to a date in the current selected month
   * this will equal to true
   */
  isInThisSelectedMonth: boolean;

  events: DateEvent[];

  /**
   * Timestamp
   */
  date: number;
}
