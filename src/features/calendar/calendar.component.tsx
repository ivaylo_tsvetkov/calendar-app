import {
  Button,
  createStyles,
  Grid,
  IconButton,
  makeStyles,
  Theme,
} from "@material-ui/core";
import React from "react";

import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectCalendarDateCellsFromSunday,
  selectCurrentDate,
} from "./calendar.selector";
import { CalendarCell } from "./components/calendar-cell/calendar-cell.component";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import {
  navigationGoNextMonth,
  navigationGoPreviousMonth,
  navigationGoToMonth,
} from "./calendar.slice";
import * as datefns from "date-fns";

const rows = [0, 1, 2, 3, 4];
const weekDays = [0, 1, 2, 3, 4, 5, 6];
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      height: "100%",
    },
    cellsContainer: {
      paddingTop: 30,
      flexGrow: 1,
    },
  })
);

export function Calendar() {
  const dispatch = useAppDispatch();
  const classes = useStyles();
  const cells = useAppSelector(selectCalendarDateCellsFromSunday);
  const currentDate = useAppSelector(selectCurrentDate);

  return (
    <div className={classes.root}>
      <Button
        color="primary"
        variant="outlined"
        onClick={() => dispatch(navigationGoToMonth(new Date()))}
      >
        Today
      </Button>
      <IconButton
        color="primary"
        component="span"
        onClick={() => dispatch(navigationGoPreviousMonth())}
      >
        <ChevronLeftIcon />
      </IconButton>

      <IconButton
        color="primary"
        component="span"
        onClick={() => {
          dispatch(navigationGoNextMonth());
        }}
      >
        <ChevronRightIcon />
      </IconButton>
      {`${datefns.format(currentDate, "MMMMMMMMM yyyy")}`}

      <div className={classes.cellsContainer}>
        <Grid container spacing={0}>
          {rows.map((r) => (
            <Grid
              key={r}
              container
              item
              spacing={0}
              style={{ justifyContent: "center" }}
            >
              <React.Fragment>
                {weekDays.map((w) => (
                  <Grid key={w} item>
                    <CalendarCell cell={cells[r * weekDays.length + w]} />
                  </Grid>
                ))}
              </React.Fragment>
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
}
