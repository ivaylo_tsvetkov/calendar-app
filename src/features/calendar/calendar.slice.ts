import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import * as datefns from "date-fns";
export interface CalendarState {
  /**
   * Timestampt
   */
  currentDate: number;
}

const initialState: CalendarState = {
  currentDate: datefns.startOfMonth(new Date()).getTime(),
};

export const calendarSlice = createSlice({
  name: "calendar",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    navigationGoToMonth: (state, { payload }: PayloadAction<Date>) => {
      const firstDayOfMonth = datefns.startOfMonth(payload);
      return { ...state, currentDate: firstDayOfMonth.getTime() };
    },
    navigationGoPreviousMonth: (state) => {
      const newDate = datefns.subMonths(state.currentDate, 1);
      return { ...state, currentDate: newDate.getTime() };
    },
    navigationGoNextMonth: (state) => {
      const newDate = datefns.addMonths(state.currentDate, 1);
      return { ...state, currentDate: newDate.getTime() };
    },
  },
});

export const {
  navigationGoToMonth,
  navigationGoPreviousMonth,
  navigationGoNextMonth,
} = calendarSlice.actions;

export default calendarSlice.reducer;
