import {
  ListItem,
  ListItemIcon,
  Checkbox,
  ListItemText,
  Popover,
  Button,
} from "@material-ui/core";
import React from "react";
import { useAppDispatch } from "../../../../app/hooks";
import { DateEvent } from "../../../events/events.interfaces";
import { attendEvent, denyEvent } from "../../../events/events.slice";

export interface CalendarCellEventProps {
  event: DateEvent;
}
export const CalendarCellEvent = ({ event }: CalendarCellEventProps) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const dispatch = useAppDispatch();

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <React.Fragment>
      <ListItem
        onClick={(e) => setAnchorEl(e.currentTarget)}
        role={undefined}
        dense
        button
      >
        <ListItemIcon>
          <Checkbox
            edge="start"
            checked={event.willAttend === "yes"}
            tabIndex={-1}
            disableRipple
          />
        </ListItemIcon>
        <ListItemText key={event.id + event.willAttend} id={event.id} primary={event.title} style={{textDecoration: event.willAttend === "no" ? "line-through" : "normal"}} />
      </ListItem>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Button
          onClick={() => {
            handleClose();
            dispatch(attendEvent(event));
          }}
        >
          Attend
        </Button>
        <Button
          onClick={() => {
            handleClose();
            dispatch(denyEvent(event));
          }}
        >
          Deny
        </Button>
      </Popover>
    </React.Fragment>
  );
};
