import {
  makeStyles,
  Theme,
  createStyles,
  Typography,
  Paper,
  List,
  TextField,
  Button,
} from "@material-ui/core";
import { useState } from "react";
import { useAppDispatch } from "../../../../app/hooks";
import { addEvent } from "../../../events/events.slice";
import { CalendarCell as CalendarCellType } from "../../calendar.interfaces";
import { CalendarCellEvent } from "../calendar-cell-event/calendar-cell-event.component";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "12vw",
      maxWidth: "12vw",
      height: 150,
      maxHeight: 150,
      overflow: "hidden",
      overflowY: "auto",
      padding: theme.spacing(1),
      textAlign: "center",
      color: theme.palette.text.secondary,
      scrollbarWidth: "none",
      scrollbarColor: "transparent",
    },
  })
);

export interface CalendarCellProps {
  cell: CalendarCellType;
}
export const CalendarCell = ({ cell }: CalendarCellProps) => {
  const dispatch = useAppDispatch();
  const classnames = useStyles();
  const [addNewEventValue, setAddNewEventValue] = useState("");

  const [isAddNewEventActive, setIsAddNewEventActive] = useState(false);
  return (
    <Paper
      onMouseEnter={() => setIsAddNewEventActive(true)}
      onMouseLeave={()=>setIsAddNewEventActive(false)}
      className={classnames.root}
    >
      <Typography>{cell.title}</Typography>
      <Typography>{cell.secondaryTitle}</Typography>

      {isAddNewEventActive && (
        <TextField
          value={addNewEventValue}
          onChange={(e) => setAddNewEventValue(e.currentTarget.value)}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              dispatch(
                addEvent({
                  date: cell.date,
                  title: addNewEventValue,
                  willAttend: "pending",
                })
              );
              setAddNewEventValue("");
              setIsAddNewEventActive(false);
            }
          }}
        />
      )}
      <List>
        {cell.events.map((e) => (
          <CalendarCellEvent key={e.id} event={e} />
        ))}
      </List>
    </Paper>
  );
};
