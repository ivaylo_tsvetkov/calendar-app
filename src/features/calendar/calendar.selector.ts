import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { CalendarCell } from "./calendar.interfaces";
import * as datefns from "date-fns";
import { DateEvent } from "../events/events.interfaces";
import { DEFAULT_CELLS_PER_PAGE } from "./calendar.constants";

export const selectCurrentDate = createSelector(
  ({ calendar: {currentDate} }: RootState) => {
    return currentDate;
  },
  (r) => r
);

const getEventsForDate = (date: Date, events: DateEvent[]) =>
  events.filter((e) =>
    datefns.isWithinInterval(e.date, {
      start: date,
      end: datefns.endOfDay(date),
    })
  );

/**
 * Selects the calendar cells that needs to be displayed
 */
export const selectCalendarDateCellsFromSunday = createSelector(
  ({ events: { events }, calendar: { currentDate } }: RootState) => {
    const startOfMonth = datefns.startOfMonth(currentDate);
    const currentDateDayOfWeek = datefns.getDay(startOfMonth);
    const neededDatesFromPreviousMonth = currentDateDayOfWeek

    const previousMonthCells: CalendarCell[] = new Array(
      neededDatesFromPreviousMonth
    )
      .fill(null)
      .map((_, index) => index + 1)
      .reverse()
      .map((i) => datefns.subDays(startOfMonth, i))
      .map((d) => {
        const cell: CalendarCell = {
          isInThisSelectedMonth: false,
          title: d.getDate().toString(),
          events: getEventsForDate(d, events),
          date: d.getTime(),
        };
        return cell;
      });

    const thisMonthCells: CalendarCell[] = new Array(
      datefns.getDaysInMonth(startOfMonth)
    )
      .fill(null)
      .map((_, index) => index)
      .map((i) => datefns.addDays(startOfMonth, i))
      .map((d) => {
        const title = datefns.isFirstDayOfMonth(d)
          ? `${datefns.format(startOfMonth, "LLL")} 1`
          : datefns.getDate(d).toString();
        const cell: CalendarCell = {
          isInThisSelectedMonth: true,
          title,
          events: getEventsForDate(d, events),
          date: d.getTime(),
        };

        return cell;
      });

    const nextMonth = datefns.addMonths(startOfMonth, 1);

    const nextMonthCellsAmount = Math.max(
      DEFAULT_CELLS_PER_PAGE -
        previousMonthCells.length -
        thisMonthCells.length,
      0
    );

    const nextMonthCells: CalendarCell[] = new Array(nextMonthCellsAmount)
      .fill(null)
      .map((_, index) => index)
      .map((i) => datefns.addDays(nextMonth, i))
      .map((d) => {
        const cell: CalendarCell = {
          isInThisSelectedMonth: false,
          title: d.getDate().toString(),
          events: getEventsForDate(d, events),
          date: d.getTime(),
        };
        return cell;
      });

    const cells: CalendarCell[] = [
      ...previousMonthCells,
      ...thisMonthCells,
      ...nextMonthCells,
    ];

    const cellsWithWeekDays = cells.map((c, i) => {
      if (i > 6) {
        return c;
      }
      const title = datefns.format(c.date, "eee");
      return { ...c, secondaryTitle: c.title, title };
    });

    return cellsWithWeekDays;
  },
  (r) => r
);
