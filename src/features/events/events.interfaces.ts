export interface DateEvent {
  id: string;
  /**
   * Timestamp
   */
  date: number;
  title: string;
  willAttend: "pending" | "no" | "yes";
}
