import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "../../app/store";
import { DateEvent } from "./events.interfaces";
export interface EventsState {
  events: DateEvent[];
}

const initialState: EventsState = {
  events: [],
};

export const eventsSlice = createSlice({
  name: "events",
  initialState,
  reducers: {
    addEvent: (state, { payload }: PayloadAction<Omit<DateEvent, "id">>) => {
      return {
        ...state,
        events: [
          ...state.events,
          { id: state.events.length.toString(), ...payload },
        ],
      };
    },
    removeEvent: (state, { payload }: PayloadAction<DateEvent>) => {
      return {
        ...state,
        events: state.events.filter((e) => e.id !== payload.id),
      };
    },
    updateEvent: (state, { payload }: PayloadAction<DateEvent>) => {
      const newEvents = state.events.map((e) => {
        if (e.id !== payload.id) {
          return e;
        }

        return payload;
      });

      return { ...state, events: newEvents };
    },
  },
});

export const { addEvent, removeEvent, updateEvent } = eventsSlice.actions;

export const attendEvent =
  (event: DateEvent): AppThunk =>
  (dispatch) => {
    dispatch(updateEvent({ ...event, willAttend: "yes" }));
  };

export const denyEvent =
  (event: DateEvent): AppThunk =>
  (dispatch) => {
    dispatch(updateEvent({ ...event, willAttend: "no" }));
  };

export default eventsSlice.reducer;
